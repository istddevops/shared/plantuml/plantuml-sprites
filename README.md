# PlantUML Sprites

## Logos

```plantuml
@startuml LogoSprites
!include https://gitlab.com/istddevops/shared/plantuml/plantuml-sprites/-/raw/main/Logos.puml
listsprites
@enduml
```

Kopieer onderstaand template om de logo-sprites te gebruiken

```template
!include https://gitlab.com/istddevops/shared/plantuml/plantuml-sprites/-/raw/main/Logos.puml
```
